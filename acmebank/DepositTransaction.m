//
//  DepositTransaction.m
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "DepositTransaction.h"

@implementation DepositTransaction

- (BOOL)dataValidated
{
    if(!self.toAccount) return NO;
    if(!self.checkFront) return NO;
    if(!self.checkBack) return NO;
    
    return YES;
}

@end
