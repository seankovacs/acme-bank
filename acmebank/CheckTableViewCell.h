//
//  CheckTableViewCell.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *checkImage;
@end
