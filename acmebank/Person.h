//
//  Person.h
//  acmebank
//
//  Created by Sean Kovacs on 4/12/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (nonatomic, copy) NSNumber *personid;
@property (nonatomic, copy) NSString *firstname;
@property (nonatomic, copy) NSString *lastname;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSNumber *isowner;

- (NSString *)prettyName;
@end
