//
//  CommonCode.h
//  acmebank
//
//  Created by Sean Kovacs on 4/12/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "fmdb/FMDatabase.h"
#import "fmdb/FMDatabaseQueue.h"
#import "AFNetworking/AFNetworking.h"
#import "NSObject+PerformBlock.h"
#import "UIView+Snapshot.h"

#import "Account.h"
#import "Person.h"
#import "ExchangeRate.h"
#import "Transaction.h"
#import "BillPayTransaction.h"
#import "TransferFundsTransaction.h"
#import "Payee.h"

@interface CommonCode : NSObject

@property (nonatomic,strong) NSString *currentUser;

+ (id)sharedInstance;

// DB
- (void)setupDatabase;
- (void)checkLogin:(NSString *)username password:(NSString *)password success:(void(^)(BOOL valid, BOOL isOwner))successBlock error:(void(^)(NSError *error))errorBlock;
- (void)getAccountsForUsername:(NSString *)username success:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock;
- (void)getAccountsForNumber:(NSNumber *)number suffix:(NSNumber *)suffix success:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock;
- (void)getAccountsWithLimit:(int)limit sorted:(BOOL)ascending accounts:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock;
- (void)getAccountsForDate:(NSDate *)date accounts:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock;
- (void)getAllCustomers:(void(^)(NSArray *persons))successBlock error:(void(^)(NSError *error))errorBlock;
- (void)getAllDates:(void(^)(NSArray *dates))successBlock error:(void(^)(NSError *error))errorBlock;

// Currency
- (void)updateExchangeRates;
- (NSArray *)getExchangeRates;
- (NSString *)convertCurrency:(NSNumber *)amount from:(NSString *)fromlocale to:(NSString *)tolocale;

// Random
- (void)showMessage:(NSString *)mess duration:(float)dur completion:(void(^)())completion;

@end
