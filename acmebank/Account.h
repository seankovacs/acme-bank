//
//  Account.h
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Account : NSObject
@property (nonatomic, copy) NSNumber *accountid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *number;
@property (nonatomic, copy) NSNumber *suffix;
@property (nonatomic, copy) NSNumber *balance;
@property (nonatomic, copy) NSNumber *personid;
@property (nonatomic, copy) NSDate *date;

- (NSString *)prettyName;
@end
