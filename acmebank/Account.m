//
//  Account.m
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "Account.h"

@implementation Account

- (NSString *)prettyName
{
    if([self.suffix intValue] == 0) {
        return [NSString stringWithFormat:@"%@ (%d)",[self.name uppercaseString],[self.number intValue]];
    }
    
    return [NSString stringWithFormat:@"%@ (%d-%d)",[self.name uppercaseString],[self.number intValue],[self.suffix intValue]];
}
@end
