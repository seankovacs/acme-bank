//
//  ItemPickerViewController.h
//  acmebank
//
//  Created by Sean Kovacs on 4/13/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemPickerItem : NSObject
@property (nonatomic,copy) NSString *title;
@property (nonatomic,strong) id object;
@end

@protocol ItemPickerViewDelegate;

@interface ItemPickerViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
@property (readwrite,assign) id<ItemPickerViewDelegate> delegate;
@property (nonatomic,strong) NSString *tag;
@property (nonatomic,strong) NSArray *sourceData;
@property (nonatomic) int selectedItem;

@end

@protocol ItemPickerViewDelegate <NSObject>
@required
- (void)itemPicker:(ItemPickerViewController *)itempicker didSelectItem:(ItemPickerItem *)item;
@end