//
//  TransferFundsTransaction.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "Transaction.h"

@interface TransferFundsTransaction : Transaction
@property (nonatomic,strong) Account *fromAccount;
@property (nonatomic,strong) Account *toAccount;

- (BOOL)dataValidated;

@end
