//
//  PayeeViewController.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Payee;
@protocol PayeeViewControllerDelegate;

@interface PayeeViewController : UIViewController
@property (readwrite,assign) id<PayeeViewControllerDelegate> delegate;
@end

@protocol PayeeViewControllerDelegate <NSObject>
@required
- (void)payeeViewController:(PayeeViewController *)payeeVC payee:(Payee *)payee;
@end