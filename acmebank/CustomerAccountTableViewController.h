//
//  CustomerAccountTableViewController.h
//  acmebank
//
//  Created by Sean Kovacs on 4/15/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerAccountTableViewController : UITableViewController

- (void)setTitle:(NSString *)title accounts:(NSArray *)accounts showDetail:(BOOL)detail;
@end
