//
//  OwnerTableViewController.h
//  acmebank
//
//  Created by Sean Kovacs on 4/15/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPickerViewController.h"

@interface OwnerTableViewController : UITableViewController <ItemPickerViewDelegate>

@end
