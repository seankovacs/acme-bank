//
//  UIView+Snapshot.m
//  acmebank
//
//  Created by Sean Kovacs on 4/13/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "UIView+Snapshot.h"
#import "UIImage+ImageEffects.h"

@implementation UIView (Snapshot)

- (UIImage *)convertViewToImage
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)setBackgroundAsFrostedView:(UIView *)frostview
{
    UIImage *currentViewImage = [frostview convertViewToImage];
    UIImageView *currentView = [[UIImageView alloc] initWithFrame:self.frame];
    currentView.image = [currentViewImage applyDarkEffect];
    
    [self insertSubview:currentView atIndex:0];
}

@end
