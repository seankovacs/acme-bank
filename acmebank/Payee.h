//
//  Payee.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Payee : NSObject
@property (nonatomic,copy) NSString *toName;
@property (nonatomic,copy) NSString *toAddress;
@property (nonatomic,copy) NSString *toCity;
@property (nonatomic,copy) NSString *toZip;
@property (nonatomic,copy) NSString *toState;
@end
