//
//  AmountViewController.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AmountViewControllerDelegate;

@interface AmountViewController : UIViewController <UITextFieldDelegate>
@property (readwrite,assign) id<AmountViewControllerDelegate> delegate;
@end

@protocol AmountViewControllerDelegate <NSObject>
@required
- (void)amountViewController:(AmountViewController *)accountVC amount:(NSNumber *)amount;
@end