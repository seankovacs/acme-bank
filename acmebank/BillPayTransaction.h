//
//  BillPayTransaction.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "Transaction.h"
#import "Payee.h"

@interface BillPayTransaction : Transaction
@property (nonatomic,strong) Account *fromAccount;
@property (nonatomic,strong) Payee *toAccount;

- (BOOL)dataValidated;

@end
