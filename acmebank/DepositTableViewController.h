//
//  DepositTableViewController.h
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPickerViewController.h"
#import "AmountViewController.h"

@class DepositTransaction;

@interface DepositTableViewController : UITableViewController <ItemPickerViewDelegate, AmountViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic,strong) DepositTransaction *transaction;

@end
