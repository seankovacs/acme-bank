//
//  MyAccountTableViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "MyAccountTableViewController.h"
#import "CommonCode.h"
#import "CustomerAccountTableViewController.h"

@interface MyAccountTableViewController ()
@property (nonatomic, strong) NSMutableArray *accounts;
@property (nonatomic, getter = isRefreshing) BOOL refreshing;
@property (nonatomic, getter = isAccountsUpdated) BOOL accountsUpdated;

- (void)refreshAccounts;
- (IBAction)changeCurrency:(id)sender;
- (IBAction)logout:(id)sender;
- (void)showDetailForNumber:(NSNumber *)number suffix:(NSNumber *)suffix;
@end

@implementation MyAccountTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self refreshAccounts];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Update if we changed currency
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)refreshAccounts
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    if(!self.accounts) self.accounts = [NSMutableArray arrayWithCapacity:0];
    
    if(!self.isAccountsUpdated && !self.isRefreshing) {
        [self.accounts removeAllObjects];
        
        [[CommonCode sharedInstance] getAccountsForUsername:[[CommonCode sharedInstance] currentUser] success:^(NSArray *accounts) {
            if(accounts && accounts.count > 0) {
                [self.accounts addObjectsFromArray:accounts];
            }
        } error:^(NSError *error) {
            NSLog(@"Error refreshing accounts: %@",error.description);
        }];
    }
}

- (IBAction)changeCurrency:(id)sender
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ItemPickerViewController *itemPickerVC = [mainBoard instantiateViewControllerWithIdentifier:@"ItemPickerViewController"];
    [itemPickerVC.view setBackgroundAsFrostedView:self.tabBarController.view];
    itemPickerVC.delegate = self;
    itemPickerVC.tag = @"ExchangeRate";
    
    NSArray *rates = [[CommonCode sharedInstance] getExchangeRates];
    NSString *currentCurrency = userDefaultStr(CURRENT_CURRENCY);
    
    NSMutableArray *items = [NSMutableArray array];
    for(ExchangeRate *rate in rates) {
        ItemPickerItem *item = [[ItemPickerItem alloc] init];
        NSString *title = [NSString stringWithFormat:@"%@ (%f)",rate.currency,[rate.rate floatValue]];
        item.title = title;
        item.object = rate;
        [items addObject:item];
        
        if([rate.currency isEqualToString:currentCurrency]) {
            itemPickerVC.selectedItem = (int)[items indexOfObject:item];
        }
    }
    
    // Alternate method of setting selectedItem
//    itemPickerVC.selectedItem = [items indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
//        ExchangeRate *item = (ExchangeRate *)[(ItemPickerItem *)obj object];
//        if([item.currency isEqualToString:currentCurrency]) {
//            *stop = YES;
//            return YES;
//        }
//        
//        return NO;
//    }];
    
    itemPickerVC.sourceData = items;
    [self presentViewController:itemPickerVC animated:NO completion:nil];
}

- (IBAction)logout:(id)sender
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *loginVC = [mainBoard instantiateViewControllerWithIdentifier:@"LoginScreen"];
    loginVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:loginVC animated:YES completion:^{
        [[[UIApplication sharedApplication] keyWindow] setRootViewController:loginVC];
    }];
}

- (void)showDetailForNumber:(NSNumber *)number suffix:(NSNumber *)suffix
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    CustomerAccountTableViewController *custAccount = [mainBoard instantiateViewControllerWithIdentifier:@"CustomerAccountTableViewController"];
    
    [[CommonCode sharedInstance] getAccountsForNumber:number suffix:suffix success:^(NSArray *accounts) {
        [custAccount setTitle:@"Detail" accounts:accounts showDetail:NO];
        
        [self.navigationController pushViewController:custAccount animated:YES];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return self.accounts.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    Account *account = [self.accounts objectAtIndex:section];
    
    return [account prettyName];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    NSString *currentCurrency = userDefaultStr(CURRENT_CURRENCY);
    
    Account *account = [self.accounts objectAtIndex:indexPath.section];
    
    cell.textLabel.text = @"Available Balance";
    cell.detailTextLabel.text = [[CommonCode sharedInstance] convertCurrency:account.balance from:@"USD" to:currentCurrency];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Account *account = [self.accounts objectAtIndex:indexPath.section];
    [self showDetailForNumber:account.number suffix:account.suffix];
}

#pragma mark - ItemPicker delegates

- (void)itemPicker:(ItemPickerViewController *)itempicker didSelectItem:(ItemPickerItem *)item
{
    if([itempicker.tag isEqualToString:@"ExchangeRate"]) {
        ExchangeRate *rate = (ExchangeRate *)item.object;
        
        setUserDefaultStr(CURRENT_CURRENCY, rate.currency);
        setUserDefaultFloat(CURRENT_CURRENCY_RATE, [rate.rate floatValue]);
        syncUserDefaults();
    }
}

@end
