//
//  PayeeViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "PayeeViewController.h"
#import "NSObject+PerformBlock.h"
#import "Payee.h"

@interface PayeeViewController ()
@property (nonatomic,weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic,weak) IBOutlet UIView *payeeViewContainer;
@property (nonatomic,weak) IBOutlet UITextField *nameField;
@property (nonatomic,weak) IBOutlet UITextField *addressField;
@property (nonatomic,weak) IBOutlet UITextField *cityField;
@property (nonatomic,weak) IBOutlet UITextField *stateField;
@property (nonatomic,weak) IBOutlet UITextField *zipField;
@property (nonatomic) BOOL isKeyboardShowing;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;
- (void)hideKeyboard;

- (void)keyboardWillShow:(NSNotification *)note;
- (void)keyboardWillHide:(NSNotification *)note;
@end

@implementation PayeeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    // Transparent UIToolbar hack... :/
    [self.toolbar setBackgroundImage:[UIImage new]
                  forToolbarPosition:UIBarPositionAny
                          barMetrics:UIBarMetricsDefault];
    [self.toolbar setShadowImage:[UIImage new]
              forToolbarPosition:UIToolbarPositionAny];
    
    self.payeeViewContainer.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.33f];
    
    [self hidePickerView:NO completion:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showPickerView:YES completion:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%@ DEALLOC",self.description);
}

- (void)hidePickerView:(BOOL)animated completion:(void(^)())completion
{
    CGRect pickerFrame = self.payeeViewContainer.frame;
    pickerFrame.origin.y = self.view.frame.size.height;
    
    [UIView animateWithDuration:(animated?0.3f:0.0f) animations:^{
        self.payeeViewContainer.frame = pickerFrame;
    } completion:^(BOOL finished) {
        if(completion) completion();
    }];
}

- (void)showPickerView:(BOOL)animated completion:(void(^)())completion
{
    CGRect pickerFrame = self.payeeViewContainer.frame;
    pickerFrame.origin.y = self.view.frame.size.height-self.payeeViewContainer.frame.size.height;
    
    [UIView animateWithDuration:(animated?0.3f:0.0f) animations:^{
        self.payeeViewContainer.frame = pickerFrame;
    } completion:^(BOOL finished) {
        if(completion) completion();
    }];
}

- (IBAction)done:(id)sender
{
    [self hideKeyboard];
    [self performBlock:^{
        [self hidePickerView:YES completion:^{
            [self dismissViewControllerAnimated:NO completion:^{
                Payee *payee = [[Payee alloc] init];
                payee.toName = self.nameField.text;
                payee.toAddress = self.addressField.text;
                payee.toCity = self.cityField.text;
                payee.toState = self.stateField.text;
                payee.toZip = self.zipField.text;
                
                if(self.delegate) [self.delegate payeeViewController:self payee:payee];
            }];
        }];
    } afterDelay:(self.isKeyboardShowing?0.20f:0.0f)];
}

- (IBAction)cancel:(id)sender
{
    [self hideKeyboard];
    [self performBlock:^{
        [self hidePickerView:YES completion:^{
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
    } afterDelay:(self.isKeyboardShowing?0.20f:0.0f)];
}

- (void)hideKeyboard
{
    [self.nameField resignFirstResponder];
    [self.addressField resignFirstResponder];
    [self.cityField resignFirstResponder];
    [self.stateField resignFirstResponder];
    [self.zipField resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardFrameForCurrentView = [self.payeeViewContainer convertRect:keyboardFrame fromView:nil];
    
    CGRect payeeViewContainerFrame = self.payeeViewContainer.frame;
    payeeViewContainerFrame.origin.y = self.view.bounds.size.height - (keyboardFrameForCurrentView.size.height + payeeViewContainerFrame.size.height);
    
    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState|(curve << 16) animations:^{
        self.payeeViewContainer.frame = payeeViewContainerFrame;
    }completion:^(BOOL finished){
        self.isKeyboardShowing = YES;
    }];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    
    CGRect payeeViewContainerFrame = self.payeeViewContainer.frame;
    payeeViewContainerFrame.origin.y = self.view.bounds.size.height - payeeViewContainerFrame.size.height;
    
    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState|curve animations:^{
        self.payeeViewContainer.frame = payeeViewContainerFrame;
    }completion:^(BOOL finished){
        self.isKeyboardShowing = NO;
    }];
}

@end
