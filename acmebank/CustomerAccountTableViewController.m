//
//  CustomerAccountTableViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/15/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "CustomerAccountTableViewController.h"
#import "CommonCode.h"

@interface CustomerAccountTableViewController ()
@property (nonatomic, strong) NSMutableArray *accounts;
@property (nonatomic) BOOL showDetail;

- (void)showDetailForNumber:(NSNumber *)username suffix:(NSNumber *)suffix;
@end

@implementation CustomerAccountTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)setTitle:(NSString *)title accounts:(NSArray *)accounts showDetail:(BOOL)detail
{
    if(!self.accounts) self.accounts = [NSMutableArray arrayWithCapacity:0];
    
    self.title = title;
    [self.accounts addObjectsFromArray:accounts];
    self.showDetail = detail;
}

- (void)showDetailForNumber:(NSNumber *)number suffix:(NSNumber *)suffix
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    CustomerAccountTableViewController *custAccount = [mainBoard instantiateViewControllerWithIdentifier:@"CustomerAccountTableViewController"];
    
    [[CommonCode sharedInstance] getAccountsForNumber:number suffix:suffix success:^(NSArray *accounts) {
        [custAccount setTitle:@"Detail" accounts:accounts showDetail:NO];
        
        [self.navigationController pushViewController:custAccount animated:YES];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return self.accounts.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    Account *account = [self.accounts objectAtIndex:section];
    
    if([account isKindOfClass:[NSDictionary class]]) {
        Account *acct = [(NSDictionary *)account objectForKey:@"Account"];
        Person *pers = [(NSDictionary *)account objectForKey:@"Person"];
        
        return [NSString stringWithFormat:@"%@ - %@",[pers prettyName],[acct prettyName]];
    }else if([self.title isEqualToString:@"Detail"]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        
        return [formatter stringFromDate:account.date];
    }
    
    return [account prettyName];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    NSString *currentCurrency = userDefaultStr(CURRENT_CURRENCY);
    
    Account *account = [self.accounts objectAtIndex:indexPath.section];
    
    if([account isKindOfClass:[NSDictionary class]]) {
        account = [(NSDictionary *)account objectForKey:@"Account"];
    }
    
    if(self.showDetail) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.textLabel.text = @"Available Balance";
    cell.detailTextLabel.text = [[CommonCode sharedInstance] convertCurrency:account.balance from:@"USD" to:currentCurrency];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Account *account = [self.accounts objectAtIndex:indexPath.section];
    if([account isKindOfClass:[Account class]]) [self showDetailForNumber:account.number suffix:account.suffix];
}


@end
