//
//  ItemPickerViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/13/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "ItemPickerViewController.h"

@implementation ItemPickerItem

@end

@interface ItemPickerViewController ()
@property (nonatomic,weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic,weak) IBOutlet UIView *pickerViewContainer;
@property (nonatomic,weak) IBOutlet UIPickerView *pickerView;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;
@end

@implementation ItemPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Transparent UIToolbar hack... :/
    [self.toolbar setBackgroundImage:[UIImage new]
                  forToolbarPosition:UIBarPositionAny
                          barMetrics:UIBarMetricsDefault];
    [self.toolbar setShadowImage:[UIImage new]
              forToolbarPosition:UIToolbarPositionAny];
    
    self.pickerViewContainer.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.33f];

    [self hidePickerView:NO completion:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showPickerView:YES completion:nil];
    
    [self.pickerView selectRow:self.selectedItem inComponent:0 animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%@ DEALLOC",self.description);
}

- (void)hidePickerView:(BOOL)animated completion:(void(^)())completion
{
    CGRect pickerFrame = self.pickerViewContainer.frame;
    pickerFrame.origin.y = self.view.frame.size.height;

    [UIView animateWithDuration:(animated?0.3f:0.0f) animations:^{
        self.pickerViewContainer.frame = pickerFrame;
    } completion:^(BOOL finished) {
        if(completion) completion();
    }];
}

- (void)showPickerView:(BOOL)animated completion:(void(^)())completion
{
    CGRect pickerFrame = self.pickerViewContainer.frame;
    pickerFrame.origin.y = self.view.frame.size.height-self.pickerViewContainer.frame.size.height;
    
    [UIView animateWithDuration:(animated?0.3f:0.0f) animations:^{
        self.pickerViewContainer.frame = pickerFrame;
    } completion:^(BOOL finished) {
        if(completion) completion();
    }];
}

- (IBAction)done:(id)sender
{
    [self hidePickerView:YES completion:^{
        [self dismissViewControllerAnimated:NO completion:^{

            int selectedIndex = (int)[self.pickerView selectedRowInComponent:0];
            
            if(self.delegate) [self.delegate itemPicker:self didSelectItem:[self.sourceData objectAtIndex:selectedIndex]];
        }];
    }];
}

- (IBAction)cancel:(id)sender
{
    [self hidePickerView:YES completion:^{
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerVie
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.sourceData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    ItemPickerItem *item = [self.sourceData objectAtIndex:row];
    return item.title;
}

@end
