//
//  DepositTransaction.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "Transaction.h"

@interface DepositTransaction : Transaction
@property (nonatomic,strong) Account *toAccount;
@property (nonatomic,strong) UIImage *checkFront;
@property (nonatomic,strong) UIImage *checkBack;

- (BOOL)dataValidated;

@end
