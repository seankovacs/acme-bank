//
//  UIView+Snapshot.h
//  acmebank
//
//  Created by Sean Kovacs on 4/13/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Snapshot)

- (UIImage *)convertViewToImage;

- (void)setBackgroundAsFrostedView:(UIView *)frostview;
@end
