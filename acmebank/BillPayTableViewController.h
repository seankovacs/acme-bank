//
//  BillPayTableViewController.h
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemPickerViewController.h"
#import "PayeeViewController.h"
#import "AmountViewController.h"

@class BillPayTransaction;

@interface BillPayTableViewController : UITableViewController <ItemPickerViewDelegate, PayeeViewControllerDelegate, AmountViewControllerDelegate>
@property (nonatomic,strong) BillPayTransaction *transaction;

@end
