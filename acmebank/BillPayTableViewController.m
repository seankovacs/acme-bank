//
//  BillPayTableViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "BillPayTableViewController.h"
#import "CommonCode.h"
#import "BillPayTransaction.h"

@interface BillPayTableViewController ()
@property (nonatomic,weak) IBOutlet UIBarButtonItem *payBillButton;
@property (nonatomic,weak) IBOutlet UITableViewCell *accountCell;
@property (nonatomic,weak) IBOutlet UITableViewCell *payeeCell;
@property (nonatomic,weak) IBOutlet UITableViewCell *amountCell;

- (IBAction)logout:(id)sender;
- (IBAction)payBill:(id)sender;
- (void)resetData;
- (void)showAccountPicker;
- (void)showPayeePicker;
- (void)showAmountPicker;
- (void)checkValidity;

@end

@implementation BillPayTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.transaction = [[BillPayTransaction alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (IBAction)logout:(id)sender
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *loginVC = [mainBoard instantiateViewControllerWithIdentifier:@"LoginScreen"];
    loginVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:loginVC animated:YES completion:^{
        [[[UIApplication sharedApplication] keyWindow] setRootViewController:loginVC];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) {
        [self showAccountPicker];
    }else if(indexPath.row == 1) {
        [self showPayeePicker];
    }else if(indexPath.row == 2) {
        [self showAmountPicker];
    }
}

- (IBAction)payBill:(id)sender
{
    [[CommonCode sharedInstance] showMessage:@"Paying bill..." duration:3.0f completion:^{
        [self resetData];
    }];
}

- (void)resetData
{
    self.transaction.fromAccount = nil;
    self.transaction.toAccount = nil;
    self.transaction.amount = nil;
    
    self.accountCell.detailTextLabel.text = nil;
    self.payeeCell.detailTextLabel.text = nil;
    self.amountCell.detailTextLabel.text = nil;
    
    self.payBillButton.enabled = NO;
}

- (void)showAccountPicker
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ItemPickerViewController *itemPickerVC = [mainBoard instantiateViewControllerWithIdentifier:@"ItemPickerViewController"];
    [itemPickerVC.view setBackgroundAsFrostedView:self.tabBarController.view];
    itemPickerVC.delegate = self;
    itemPickerVC.tag = @"Account";
    
    [[CommonCode sharedInstance] getAccountsForUsername:[[CommonCode sharedInstance] currentUser] success:^(NSArray *accounts) {
        NSMutableArray *items = [NSMutableArray array];
        for(Account *account in accounts) {
            ItemPickerItem *item = [[ItemPickerItem alloc] init];
            item.title = [account prettyName];
            item.object = account;
            [items addObject:item];
        }
        
        itemPickerVC.sourceData = items;
        [self presentViewController:itemPickerVC animated:NO completion:nil];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

- (void)showPayeePicker
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PayeeViewController *payeeVC = [mainBoard instantiateViewControllerWithIdentifier:@"PayeeViewController"];
    [payeeVC.view setBackgroundAsFrostedView:self.tabBarController.view];
    payeeVC.delegate = self;

    [self presentViewController:payeeVC animated:NO completion:nil];
}

- (void)showAmountPicker
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    AmountViewController *amountVC = [mainBoard instantiateViewControllerWithIdentifier:@"AmountViewController"];
    [amountVC.view setBackgroundAsFrostedView:self.tabBarController.view];
    amountVC.delegate = self;
    
    [self presentViewController:amountVC animated:NO completion:nil];
}

- (void)checkValidity
{
    self.payBillButton.enabled = [self.transaction dataValidated];
}

#pragma mark - ItemPicker delegates

- (void)itemPicker:(ItemPickerViewController *)itempicker didSelectItem:(ItemPickerItem *)item
{
    if([itempicker.tag isEqualToString:@"Account"]) {
        Account *account = (Account *)item.object;
        self.transaction.fromAccount = account;
        self.accountCell.detailTextLabel.text = [account prettyName];
    }
    
    [self checkValidity];
}

#pragma mark - PayeeVC delegates

- (void)payeeViewController:(PayeeViewController *)payeeVC payee:(Payee *)payee
{
    self.transaction.toAccount = payee;
    
    self.payeeCell.detailTextLabel.text = payee.toName;
    
    [self checkValidity];
}

#pragma mark - Amount delegate

- (void)amountViewController:(AmountViewController *)accountVC amount:(NSNumber *)amount
{   
    self.transaction.amount = amount;
    
    self.amountCell.detailTextLabel.text = [[CommonCode sharedInstance] convertCurrency:amount from:@"USD" to:@"USD"];
    
    [self checkValidity];
}

@end
