//
//  OwnerTableViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/15/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "OwnerTableViewController.h"
#import "CommonCode.h"
#import "CustomerAccountTableViewController.h"

@interface OwnerTableViewController ()
- (IBAction)logout:(id)sender;
- (void)showCustomerPicker;
- (void)showCustomerAccount:(NSString *)username;
- (void)showCustomersWithLimit:(int)limit sort:(BOOL)ascending;
- (void)showDatePicker;
- (void)showCustomersWithDate:(NSDate *)date;

@end

@implementation OwnerTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logout:(id)sender
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *loginVC = [mainBoard instantiateViewControllerWithIdentifier:@"LoginScreen"];
    loginVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:loginVC animated:YES completion:^{
        [[[UIApplication sharedApplication] keyWindow] setRootViewController:loginVC];
    }];
}

#pragma mark - Table view data source


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) {
        [self showCustomerPicker];
    }else if(indexPath.row == 1) {
        [self showCustomersWithLimit:5 sort:NO];
    }else if(indexPath.row == 2) {
        [self showCustomersWithLimit:5 sort:YES];
    }else if(indexPath.row == 3) {
        [self showDatePicker];
    }
}

- (void)showCustomerPicker
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ItemPickerViewController *itemPickerVC = [mainBoard instantiateViewControllerWithIdentifier:@"ItemPickerViewController"];
    [itemPickerVC.view setBackgroundAsFrostedView:self.navigationController.view];
    itemPickerVC.delegate = self;
    itemPickerVC.tag = @"Customer";
    
    [[CommonCode sharedInstance] getAllCustomers:^(NSArray *persons) {
        NSMutableArray *items = [NSMutableArray array];
        for(Person *person in persons) {
            ItemPickerItem *item = [[ItemPickerItem alloc] init];
            item.title = [person prettyName];
            item.object = person;
            [items addObject:item];
        }
        
        itemPickerVC.sourceData = items;
        [self presentViewController:itemPickerVC animated:NO completion:nil];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

- (void)showCustomerAccount:(NSString *)username
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    CustomerAccountTableViewController *custAccount = [mainBoard instantiateViewControllerWithIdentifier:@"CustomerAccountTableViewController"];
    
    [[CommonCode sharedInstance] getAccountsForUsername:username success:^(NSArray *accounts) {
        [custAccount setTitle:@"Customer Account" accounts:accounts showDetail:YES];
        
        [self.navigationController pushViewController:custAccount animated:YES];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

- (void)showCustomersWithLimit:(int)limit sort:(BOOL)ascending
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    CustomerAccountTableViewController *custAccount = [mainBoard instantiateViewControllerWithIdentifier:@"CustomerAccountTableViewController"];
    
    [[CommonCode sharedInstance] getAccountsWithLimit:limit sorted:ascending accounts:^(NSArray *accounts) {
        [custAccount setTitle:@"Customer Accounts" accounts:accounts showDetail:NO];
        
        [self.navigationController pushViewController:custAccount animated:YES];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

- (void)showDatePicker
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ItemPickerViewController *itemPickerVC = [mainBoard instantiateViewControllerWithIdentifier:@"ItemPickerViewController"];
    [itemPickerVC.view setBackgroundAsFrostedView:self.navigationController.view];
    itemPickerVC.delegate = self;
    itemPickerVC.tag = @"Date";
    
    [[CommonCode sharedInstance] getAllDates:^(NSArray *dates) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        
        NSMutableArray *items = [NSMutableArray array];
        for(NSDate *date in dates) {
            ItemPickerItem *item = [[ItemPickerItem alloc] init];
            item.title = [formatter stringFromDate:date];
            item.object = date;
            [items addObject:item];
        }
        
        itemPickerVC.sourceData = items;
        [self presentViewController:itemPickerVC animated:NO completion:nil];
    } error:^(NSError *error) {
        NSLog(@"Error getting dates: %@",error.description);
    }];
}

- (void)showCustomersWithDate:(NSDate *)date
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    CustomerAccountTableViewController *custAccount = [mainBoard instantiateViewControllerWithIdentifier:@"CustomerAccountTableViewController"];
    
    [[CommonCode sharedInstance] getAccountsForDate:date accounts:^(NSArray *accounts) {
        [custAccount setTitle:@"Customer Accounts" accounts:accounts showDetail:NO];
        
        [self.navigationController pushViewController:custAccount animated:YES];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

#pragma mark - ItemPicker delegates

- (void)itemPicker:(ItemPickerViewController *)itempicker didSelectItem:(ItemPickerItem *)item
{
    if([itempicker.tag isEqualToString:@"Customer"]) {
        Person *person = (Person *)item.object;
        
        [self showCustomerAccount:person.username];
    }else if([itempicker.tag isEqualToString:@"Date"]) {
        NSDate *date = (NSDate *)item.object;
        
        [self showCustomersWithDate:date];
    }
}


@end
