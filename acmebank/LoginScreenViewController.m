//
//  LoginScreenViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/15/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "LoginScreenViewController.h"
#import "CommonCode.h"

@interface LoginScreenViewController ()

@property (nonatomic,weak) IBOutlet UITextField *usernameField;
@property (nonatomic,weak) IBOutlet UITextField *passwordField;

- (IBAction)login:(id)sender;
@end

@implementation LoginScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (IBAction)login:(id)sender
{
    [[CommonCode sharedInstance] checkLogin:self.usernameField.text password:self.passwordField.text success:^(BOOL valid, BOOL isOwner) {
        if(valid) {
            [[CommonCode sharedInstance] setCurrentUser:self.usernameField.text];
            
            UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            UIViewController *tabVC = [mainBoard instantiateViewControllerWithIdentifier:(isOwner?@"OwnerScreen":@"MainCustomer")];
            tabVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [self presentViewController:tabVC animated:YES completion:^{
                [[[UIApplication sharedApplication] keyWindow] setRootViewController:tabVC];
            }];
            
        }else {
            [[CommonCode sharedInstance] showMessage:@"Invalid username and/or password." duration:1.0f completion:nil];
        }
        
    } error:^(NSError *error) {
        [[CommonCode sharedInstance] showMessage:@"Invalid username and/or password." duration:1.0f completion:nil];
    }];
}

@end
