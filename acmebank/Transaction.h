//
//  Transaction.h
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Account;

@interface Transaction : NSObject

@property (nonatomic,copy) NSNumber *amount;

@end
