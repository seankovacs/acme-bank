//
//  ExchangeRate.h
//  acmebank
//
//  Created by Sean Kovacs on 4/12/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExchangeRate : NSObject
@property (nonatomic, copy) NSNumber *exchangerateid;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSNumber *rate;
@end
