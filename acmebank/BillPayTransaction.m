//
//  BillPayTransaction.m
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "BillPayTransaction.h"

@implementation BillPayTransaction

- (BOOL)dataValidated
{
    if(!self.fromAccount) return NO;
    if(!self.toAccount.toName.length > 0) return NO;
    if(!self.toAccount.toAddress.length > 0) return NO;
    if(!self.toAccount.toCity.length > 0) return NO;
    if(!self.toAccount.toState.length > 0) return NO;
    if(!self.toAccount.toZip.length > 0) return NO;
    if(self.amount.doubleValue == 0) return NO;
    
    return YES;
}


@end
