//
//  TransferFundsTransaction.m
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "TransferFundsTransaction.h"

@implementation TransferFundsTransaction

- (BOOL)dataValidated
{
    if(!self.fromAccount) return NO;
    if(!self.toAccount) return NO;
    if(self.amount.doubleValue == 0) return NO;
    
    return YES;
}

@end
