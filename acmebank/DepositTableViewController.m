//
//  DepositTableViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "DepositTableViewController.h"
#import "CheckTableViewCell.h"
#import "CommonCode.h"
#import "DepositTransaction.h"
#import "UIImage+Crop.h"

typedef enum {
    checkSideFront = 1,
    checkSideBack = 2,
} checkSide;

@interface DepositTableViewController ()
@property (nonatomic,weak) IBOutlet UIBarButtonItem *depositButton;
@property (nonatomic,weak) IBOutlet UITableViewCell *depositToCell;
@property (nonatomic,weak) IBOutlet UITableViewCell *amountCell;
@property (nonatomic,weak) IBOutlet CheckTableViewCell *checkFrontCell;
@property (nonatomic,weak) IBOutlet CheckTableViewCell *checkBackCell;

@property (nonatomic,strong) UIImagePickerController *imager;

- (IBAction)logout:(id)sender;
- (IBAction)deposit:(id)sender;
- (void)resetData;
- (void)showAccountPicker;
- (void)showAmountPicker;
- (void)showPictureTaker:(checkSide)side;
- (void)closePictureTaker;
- (void)processCheckImage:(UIImage *)image forSide:(checkSide)side;
- (void)checkValidity;
@end

@implementation DepositTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.transaction = [[DepositTransaction alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (IBAction)logout:(id)sender
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *loginVC = [mainBoard instantiateViewControllerWithIdentifier:@"LoginScreen"];
    loginVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:loginVC animated:YES completion:^{
        [[[UIApplication sharedApplication] keyWindow] setRootViewController:loginVC];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0 && indexPath.section == 0) {
        [self showAccountPicker];
    }else if(indexPath.row == 1 && indexPath.section == 0) {
        [self showAmountPicker];
    }else if(indexPath.row == 0 && indexPath.section == 1) {
        [self showPictureTaker:checkSideFront];
    }else if(indexPath.row == 1 && indexPath.section == 1) {
        [self showPictureTaker:checkSideBack];
    }
}

- (IBAction)deposit:(id)sender
{
    [[CommonCode sharedInstance] showMessage:@"Depositing funds..." duration:3.0f completion:^{
        [self resetData];
    }];
}

- (void)resetData
{
    self.transaction.toAccount = nil;
    self.transaction.checkFront = nil;
    self.transaction.checkBack = nil;
    
    self.depositToCell.detailTextLabel.text = nil;
    self.amountCell.detailTextLabel.text = nil;
    self.checkFrontCell.checkImage.image = nil;
    self.checkBackCell.checkImage.image = nil;
    
    self.depositButton.enabled = NO;
}

- (void)showAccountPicker
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ItemPickerViewController *itemPickerVC = [mainBoard instantiateViewControllerWithIdentifier:@"ItemPickerViewController"];
    [itemPickerVC.view setBackgroundAsFrostedView:self.tabBarController.view];
    itemPickerVC.delegate = self;
    itemPickerVC.tag = @"Account";
    
    [[CommonCode sharedInstance] getAccountsForUsername:[[CommonCode sharedInstance] currentUser] success:^(NSArray *accounts) {
        NSMutableArray *items = [NSMutableArray array];
        for(Account *account in accounts) {
            ItemPickerItem *item = [[ItemPickerItem alloc] init];
            item.title = [account prettyName];
            item.object = account;
            [items addObject:item];
        }
        
        itemPickerVC.sourceData = items;
        [self presentViewController:itemPickerVC animated:NO completion:nil];
    } error:^(NSError *error) {
        NSLog(@"Error getting accounts: %@",error.description);
    }];
}

- (void)showAmountPicker
{
    UIStoryboard *mainBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    AmountViewController *amountVC = [mainBoard instantiateViewControllerWithIdentifier:@"AmountViewController"];
    [amountVC.view setBackgroundAsFrostedView:self.tabBarController.view];
    amountVC.delegate = self;
    
    [self presentViewController:amountVC animated:NO completion:nil];
}

- (void)showPictureTaker:(checkSide)side
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        if(!self.imager) {
            self.imager = [[UIImagePickerController alloc] init];
        }
        self.imager.delegate = self;
        self.imager.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imager.showsCameraControls = NO;
        self.imager.view.tag = side; // Easy hack to pass along var
        
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        float cameraAspectRatio = 4.0 / 3.0;
        float imageWidth = floorf(screenSize.width * cameraAspectRatio);
        float scale = ceilf((screenSize.height / imageWidth) * 10.0) / 10.0;
        
        self.imager.cameraViewTransform = CGAffineTransformMakeTranslation(0, (screenSize.height - imageWidth) / 2.0);
        self.imager.cameraViewTransform = CGAffineTransformScale(self.imager.cameraViewTransform, scale, scale);
        
        UIView *overlay = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.imager.view.bounds.size.width, self.imager.view.bounds.size.height)];
        overlay.backgroundColor = [UIColor clearColor];
        
        UIView *checkRect = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.imager.view.bounds.size.width/1.5, self.imager.view.bounds.size.height/1.3)];
        checkRect.backgroundColor = [UIColor clearColor];
        checkRect.center = self.imager.view.center;
        checkRect.layer.borderColor = [UIColor greenColor].CGColor;
        checkRect.layer.borderWidth = 4.0f;
        
        [overlay addSubview:checkRect];
        
        UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, self.imager.view.bounds.size.height-44.0f, self.imager.view.bounds.size.width, 44.0f)];
        
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closePictureTaker)];
        UIBarButtonItem *snap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self.imager action:@selector(takePicture)];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        [toolBar setItems:@[cancel,flex,snap,flex]];
        
        [overlay addSubview:toolBar];
        
        self.imager.cameraOverlayView = overlay;
        
        [self presentViewController:self.imager animated:NO completion:nil];
    }
}

- (void)closePictureTaker
{
    [self.imager dismissViewControllerAnimated:YES completion:nil];
    self.imager = nil;
}

- (void)processCheckImage:(UIImage *)image forSide:(checkSide)side
{
    [self performBlockOnBGTheadBackgroundPriority:^{
        CGSize imagerSize = image.size;
        CGFloat width = imagerSize.width/2.25;
        CGFloat height = imagerSize.height/1.4;
        
        UIImage *croppedImage =[UIImage imageWithImage:[image horizontalImage] cropInRect:CGRectMake((imagerSize.height/2.0f)-(height/2.0f), (imagerSize.width/2.0f)-(width/2.0f), height, width)];
        
        [self performBlockOnMainThead:^{
            if(side == checkSideFront) {
                self.transaction.checkFront = croppedImage;
                self.checkFrontCell.checkImage.image = croppedImage;
            }
            if(side == checkSideBack) {
                self.transaction.checkBack = croppedImage;
                self.checkBackCell.checkImage.image = croppedImage;
            }
            
            [self checkValidity];
        }];
    }];
}

- (void)checkValidity
{
    self.depositButton.enabled = [self.transaction dataValidated];
}

#pragma mark - ItemPicker delegates

- (void)itemPicker:(ItemPickerViewController *)itempicker didSelectItem:(ItemPickerItem *)item
{
    if([itempicker.tag isEqualToString:@"Account"]) {
        Account *account = (Account *)item.object;
        self.transaction.toAccount = account;
        self.depositToCell.detailTextLabel.text = [account prettyName];
    }
    
    [self checkValidity];
}

#pragma mark - Amount delegate

- (void)amountViewController:(AmountViewController *)accountVC amount:(NSNumber *)amount
{
    self.transaction.amount = amount;
    
    self.amountCell.detailTextLabel.text = [[CommonCode sharedInstance] convertCurrency:amount from:@"USD" to:@"USD"];
    
    [self checkValidity];
}

#pragma mark - photo picker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if(picker.view.tag > 0) [self processCheckImage:originalImage forSide:picker.view.tag];

    [self closePictureTaker];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self closePictureTaker];
}

@end
