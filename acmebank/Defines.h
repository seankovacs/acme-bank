//
//  Defines.h
//  acmebank
//
//  Created by Sean Kovacs on 4/11/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#define CURRENT_CURRENCY @"CURRENT_CURRENCY"
#define CURRENT_CURRENCY_RATE @"CURRENT_CURRENCY_RATE"

#define userDefaultStr(x) [[NSUserDefaults standardUserDefaults] valueForKey:x]
#define setUserDefaultStr(x,y) [[NSUserDefaults standardUserDefaults] setValue:y forKey:x]
#define userDefaultBool(x) [[NSUserDefaults standardUserDefaults] boolForKey:x]
#define setUserDefaultBool(x,y) [[NSUserDefaults standardUserDefaults] setBool:y forKey:x]
#define userDefaultObj(x) [[NSUserDefaults standardUserDefaults] objectForKey:x]
#define setUserDefaultObj(x,y) [[NSUserDefaults standardUserDefaults] setObject:y forKey:x]
#define userDefaultInt(x) [[NSUserDefaults standardUserDefaults] integerForKey:x]
#define setUserDefaultInt(x,y) [[NSUserDefaults standardUserDefaults] setInteger:y forKey:x]
#define userDefaultFloat(x) [[NSUserDefaults standardUserDefaults] floatForKey:x]
#define setUserDefaultFloat(x,y) [[NSUserDefaults standardUserDefaults] setFloat:y forKey:x]
#define syncUserDefaults() [[NSUserDefaults standardUserDefaults] synchronize]
