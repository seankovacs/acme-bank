//
//  CommonCode.m
//  acmebank
//
//  Created by Sean Kovacs on 4/12/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "CommonCode.h"

@interface CommonCode ()

- (NSString *)dbPath;

@end

@implementation CommonCode

+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark DB
- (NSString *)dbPath
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"acmebank.sqlite"];
}

- (void)setupDatabase
{
    // Check to see if the db in the app bundle has been copied to the documents directory (req. for read/write)
    if(![[NSFileManager defaultManager] fileExistsAtPath:[self dbPath]]) {
        NSLog(@"db doesn't exist, copying");
        
        NSString *bundleDB = [[NSBundle mainBundle] pathForResource:@"acmebank" ofType:@"sqlite"];
        
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:bundleDB toPath:[self dbPath] error:&error];
        
        if(error) {
            NSLog(@"Error saving db to documents directory: %@",error.description);
        }
    }
    
    if(!userDefaultStr(CURRENT_CURRENCY)) {
        setUserDefaultStr(CURRENT_CURRENCY, @"USD");
        setUserDefaultFloat(CURRENT_CURRENCY_RATE, 1.0f);
    }
}

- (void)checkLogin:(NSString *)username password:(NSString *)password success:(void(^)(BOOL valid, BOOL isOwner))successBlock error:(void(^)(NSError *error))errorBlock
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        FMResultSet *results = [database executeQuery:@"select *\
                                                        from persons\
                                                        where persons.username = ?\
                                                        and persons.password = ?",username,password];
        
        int count = 0;
        BOOL admin = NO;
        
        while([results next]) {
            count += 1;
            if([results boolForColumn:@"isowner"]) admin = YES;
        }
        [database close];
        
        if(successBlock) successBlock(count>0,admin);
    }else {
        NSError *error = [NSError errorWithDomain:@"1" code:1 userInfo:nil];
        if(errorBlock) errorBlock(error);
    }
}

- (void)getAccountsForUsername:(NSString *)username success:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        NSMutableArray *accounts = [NSMutableArray array];
        
        FMResultSet *results = [database executeQuery:@"select accounts.*\
                                                        from accounts\
                                                            inner join persons\
                                                            on accounts.personid = persons.personid\
                                                            inner join (\
                                                                        select accounts.personid, accounts.number, accounts.suffix, max(accounts.date) maxdate\
                                                                        from accounts\
                                                                        group by accounts.personid, accounts.number, accounts.suffix\
                                                                        ) currentbalance\
                                                            on accounts.personid = currentbalance.personid\
                                                            and accounts.number = currentbalance.number\
                                                            and accounts.suffix = currentbalance.suffix\
                                                            and accounts.date = currentbalance.maxdate\
                                                        where persons.username = ?\
                                                        order by accounts.name",username];

        while([results next]) {
            Account *account	= [[Account alloc] init];
            account.accountid = [NSNumber numberWithInt:[results intForColumn:@"accountid"]];
            account.name = [results stringForColumn:@"name"];
            account.number = [NSNumber numberWithInt:[results intForColumn:@"number"]];
            account.suffix = [NSNumber numberWithInt:[results intForColumn:@"suffix"]];
            account.balance = [NSNumber numberWithDouble:[results doubleForColumn:@"balance"]];
            account.personid = [NSNumber numberWithInt:[results intForColumn:@"personid"]];

            [accounts addObject:account];
        }
        [database close];
        
        if(successBlock) successBlock(accounts);
    }else {
        NSError *error = [NSError errorWithDomain:@"1" code:1 userInfo:nil];
        if(errorBlock) errorBlock(error);
    }
}

- (void)getAccountsForNumber:(NSNumber *)number suffix:(NSNumber *)suffix success:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        NSMutableArray *accounts = [NSMutableArray array];
        
        FMResultSet *results = [database executeQuery:@"select accounts.*\
                                from accounts\
                                where accounts.number = ?\
                                and accounts.suffix = ?\
                                order by accounts.date desc",number,suffix];
        
        while([results next]) {
            Account *account	= [[Account alloc] init];
            account.accountid = [NSNumber numberWithInt:[results intForColumn:@"accountid"]];
            account.name = [results stringForColumn:@"name"];
            account.number = [NSNumber numberWithInt:[results intForColumn:@"number"]];
            account.suffix = [NSNumber numberWithInt:[results intForColumn:@"suffix"]];
            account.balance = [NSNumber numberWithDouble:[results doubleForColumn:@"balance"]];
            account.personid = [NSNumber numberWithInt:[results intForColumn:@"personid"]];
            account.date = [results dateForColumn:@"date"];
            
            [accounts addObject:account];
        }
        [database close];
        
        if(successBlock) successBlock(accounts);
    }else {
        NSError *error = [NSError errorWithDomain:@"1" code:1 userInfo:nil];
        if(errorBlock) errorBlock(error);
    }
}

- (void)getAccountsWithLimit:(int)limit sorted:(BOOL)ascending accounts:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        NSMutableArray *accounts = [NSMutableArray array];
        
        FMResultSet *results = [database executeQuery:[NSString stringWithFormat:@"select aggregateaccount.*, 'all' name, persons.*\
                                                                                   from (\
                                                                                         select accounts.personid, accounts.date, accounts.number, 'all' suffix, sum(accounts.balance) balance\
                                                                                         from accounts\
                                                                                         group by accounts.personid, accounts.date, accounts.number\
                                                                                         ) aggregateaccount\
                                                                                       inner join persons\
                                                                                       on aggregateaccount.personid = persons.personid\
                                                                                       inner join (\
                                                                                                   select accounts.personid, accounts.number, max(accounts.date) maxdate\
                                                                                                   from accounts\
                                                                                                   group by accounts.personid, accounts.number\
                                                                                                   ) currentbalance\
                                                                                       on aggregateaccount.personid = currentbalance.personid\
                                                                                       and aggregateaccount.number = currentbalance.number\
                                                                                       and aggregateaccount.date = currentbalance.maxdate\
                                                                                   where persons.isowner = 0\
                                                                                   order by aggregateaccount.balance %@\
                                                                                   limit %d",(ascending?@"asc":@"desc"),limit]];
        
        while([results next]) {
            Account *account = [[Account alloc] init];
            account.name = [results stringForColumn:@"name"];
            account.number = [NSNumber numberWithInt:[results intForColumn:@"number"]];
            account.suffix = [NSNumber numberWithInt:[results intForColumn:@"suffix"]];
            account.balance = [NSNumber numberWithDouble:[results doubleForColumn:@"balance"]];
            account.personid = [NSNumber numberWithInt:[results intForColumn:@"personid"]];

            Person *person = [[Person alloc] init];
            person.firstname = [results stringForColumn:@"firstname"];
            person.lastname = [results stringForColumn:@"lastname"];
            person.username = [results stringForColumn:@"username"];
            
            [accounts addObject:@{@"Account": account, @"Person": person}];
        }
        [database close];
        
        if(successBlock) successBlock(accounts);
    }else {
        NSError *error = [NSError errorWithDomain:@"1" code:1 userInfo:nil];
        if(errorBlock) errorBlock(error);
    }
}

- (void)getAccountsForDate:(NSDate *)date accounts:(void(^)(NSArray *accounts))successBlock error:(void(^)(NSError *error))errorBlock
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        NSMutableArray *accounts = [NSMutableArray array];
        
        FMResultSet *results = [database executeQuery:@"select aggregateaccount.*, 'all' name, persons.*\
                                                        from (\
                                                              select accounts.personid, accounts.date, accounts.number, 'all' suffix, sum(accounts.balance) balance\
                                                              from accounts\
                                                              group by accounts.personid, accounts.date, accounts.number\
                                                              ) aggregateaccount\
                                                        inner join persons\
                                                        on aggregateaccount.personid = persons.personid\
                                                        where persons.isowner = 0\
                                                        and aggregateaccount.date = ?\
                                                        order by lastname",date];
        
        while([results next]) {
            Account *account = [[Account alloc] init];
            account.name = [results stringForColumn:@"name"];
            account.number = [NSNumber numberWithInt:[results intForColumn:@"number"]];
            account.suffix = [NSNumber numberWithInt:[results intForColumn:@"suffix"]];
            account.balance = [NSNumber numberWithDouble:[results doubleForColumn:@"balance"]];
            account.personid = [NSNumber numberWithInt:[results intForColumn:@"personid"]];
            
            Person *person = [[Person alloc] init];
            person.firstname = [results stringForColumn:@"firstname"];
            person.lastname = [results stringForColumn:@"lastname"];
            person.username = [results stringForColumn:@"username"];
            
            [accounts addObject:@{@"Account": account, @"Person": person}];
        }
        [database close];
        
        if(successBlock) successBlock(accounts);
    }else {
        NSError *error = [NSError errorWithDomain:@"1" code:1 userInfo:nil];
        if(errorBlock) errorBlock(error);
    }
}

- (void)getAllCustomers:(void(^)(NSArray *persons))successBlock error:(void(^)(NSError *error))errorBlock
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        NSMutableArray *persons = [NSMutableArray array];
        
        FMResultSet *results = [database executeQuery:@"select *\
                                                        from persons\
                                                        where persons.isowner = 0\
                                                        order by lastname"];
        
        while([results next]) {
            Person *person	= [[Person alloc] init];
            person.personid = [NSNumber numberWithInt:[results intForColumn:@"personid"]];
            person.firstname = [results stringForColumn:@"firstname"];
            person.lastname = [results stringForColumn:@"lastname"];
            person.username = [results stringForColumn:@"username"];
            
            [persons addObject:person];
        }
        [database close];
        
        if(successBlock) successBlock(persons);
    }else {
        NSError *error = [NSError errorWithDomain:@"1" code:1 userInfo:nil];
        if(errorBlock) errorBlock(error);
    }
}

- (void)getAllDates:(void(^)(NSArray *dates))successBlock error:(void(^)(NSError *error))errorBlock
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        NSMutableArray *dates = [NSMutableArray array];
        
        FMResultSet *results = [database executeQuery:@"select distinct date\
                                                        from accounts\
                                                        order by date desc"];
        
        while([results next]) {
            [dates addObject:[results dateForColumn:@"date"]];
        }
        [database close];
        
        if(successBlock) successBlock(dates);
    }else {
        NSError *error = [NSError errorWithDomain:@"1" code:1 userInfo:nil];
        if(errorBlock) errorBlock(error);
    }
}

#pragma mark Currency
- (void)updateExchangeRates
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://openexchangerates.org/api/latest.json?app_id=99f1ed49eccd47af9125f0db5f995c34"]];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Exchange rates downloaded");
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        //NSLog(@"Data: %@",json.description);
        
        NSDictionary *rates = [json valueForKeyPath:@"rates"];
        
        // Don't wipe out what we have stored if what we get is nil
        if(rates.count > 0) {
            [self performBlockOnBGTheadBackgroundPriority:^{
                FMDatabaseQueue *database = [FMDatabaseQueue databaseQueueWithPath:[self dbPath]];
                [database inDatabase:^(FMDatabase *db) {
                    [db executeUpdate:@"DELETE FROM exchangerate"];
                    
                    for(NSString *keyRate in [rates keyEnumerator]) {
                        [db executeUpdate:@"INSERT INTO exchangerate (currency, rate) VALUES (?, ?)", keyRate, [rates valueForKey:keyRate], nil];
                    }
                    
                    [db close];
                }];
                
                NSLog(@"Exchange rates updated");
            }];
            
        }
        
    } failure:nil];
    [op start];
}

- (NSArray *)getExchangeRates
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dbPath]];
    if([database open]) {
        NSMutableArray *rates = [NSMutableArray array];
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM exchangerate ORDER BY currency"];
        
        while([results next]) {
            ExchangeRate *rate	= [[ExchangeRate alloc] init];
            rate.exchangerateid = [NSNumber numberWithInt:[results intForColumn:@"exchangerateid"]];
            rate.currency = [results stringForColumn:@"currency"];
            rate.rate = [NSNumber numberWithFloat:[results doubleForColumn:@"rate"]];
            
            [rates addObject:rate];
        }
        [database close];
        
        return rates;
    }
    
    return nil;
}

- (NSString *)convertCurrency:(NSNumber *)amount from:(NSString *)from to:(NSString *)to
{
    float fromAmount = [amount floatValue];
    float exrate = userDefaultFloat(CURRENT_CURRENCY_RATE);
    float toAmount = fromAmount * exrate;
    
    NSDecimalNumber *price = [[NSDecimalNumber alloc] initWithFloat:toAmount];

    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setCurrencyCode:to];
    NSString *format = [currencyFormatter positiveFormat];
    [currencyFormatter setPositiveFormat:format];
    
    return [currencyFormatter stringFromNumber:price];
}

#pragma mark Random
- (void)showMessage:(NSString *)mess duration:(float)dur completion:(void(^)())completion
{
    CGRect frame1 = [[UIApplication sharedApplication] delegate].window.frame;
    
    CGRect textSize = [mess boundingRectWithSize:CGSizeMake(200.0, 200.0) options:0 attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0]} context:nil];
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    
    UIView *messageContainer = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, frame1.size.width, frame1.size.height)];
    messageContainer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UIView *messageBG = [[UIView alloc] initWithFrame:CGRectMake(0, 0, textSize.size.width+25.0, textSize.size.height+50.0)];
    messageBG.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    messageBG.center = messageContainer.center;
    messageBG.backgroundColor = [UIColor lightGrayColor];

    messageBG.layer.cornerRadius = 7.0;
    messageBG.layer.masksToBounds = YES;
    messageBG.layer.opacity = 0.75;

    [messageContainer addSubview:messageBG];
    UILabel *messageText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textSize.size.width, textSize.size.height)];
    messageText.center = messageBG.center;
    messageText.backgroundColor = [UIColor clearColor];
    messageText.textColor = [UIColor whiteColor];
    messageText.font = [UIFont boldSystemFontOfSize:14];
    messageText.numberOfLines = 2;
    messageText.textAlignment = NSTextAlignmentCenter;
    messageText.text = mess;
    [messageContainer addSubview:messageText];
    
    [window addSubview:messageContainer];

    messageContainer.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
    messageContainer.alpha = 0.25f;
    
    [UIView animateWithDuration:0.66f delay:0.0f usingSpringWithDamping:0.5f initialSpringVelocity:0.66f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        messageContainer.transform = CGAffineTransformIdentity;
        messageContainer.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [self performBlock:^{
            [UIView animateWithDuration:0.33f delay:0.0f usingSpringWithDamping:0.5f initialSpringVelocity:0.66f options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                messageContainer.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
                messageContainer.alpha = 0.0f;
            } completion:^(BOOL finished) {
                [messageContainer removeFromSuperview];
                if(completion) completion();
            }];
        } afterDelay:dur];
    }];
}

@end
