//
//  AmountViewController.m
//  acmebank
//
//  Created by Sean Kovacs on 4/14/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "AmountViewController.h"
#import "NSObject+PerformBlock.h"
#import "CommonCode.h"

@interface AmountViewController ()
@property (nonatomic,weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic,weak) IBOutlet UIView *amountViewContainer;
@property (nonatomic,weak) IBOutlet UITextField *amountField;
@property (nonatomic) BOOL isKeyboardShowing;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;
- (void)hideKeyboard;

- (void)keyboardWillShow:(NSNotification *)note;
- (void)keyboardWillHide:(NSNotification *)note;
@end

@implementation AmountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    // Transparent UIToolbar hack... :/
    [self.toolbar setBackgroundImage:[UIImage new]
                  forToolbarPosition:UIBarPositionAny
                          barMetrics:UIBarMetricsDefault];
    [self.toolbar setShadowImage:[UIImage new]
              forToolbarPosition:UIToolbarPositionAny];
    
    self.amountViewContainer.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.33f];
    
    self.amountField.text = @"$ ";
    
    [self hidePickerView:NO completion:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showPickerView:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    NSLog(@"%@ DEALLOC",self.description);
}

- (void)hidePickerView:(BOOL)animated completion:(void(^)())completion
{
    CGRect pickerFrame = self.amountViewContainer.frame;
    pickerFrame.origin.y = self.view.frame.size.height;
    
    [UIView animateWithDuration:(animated?0.3f:0.0f) animations:^{
        self.amountViewContainer.frame = pickerFrame;
    } completion:^(BOOL finished) {
        if(completion) completion();
    }];
}

- (void)showPickerView:(BOOL)animated completion:(void(^)())completion
{
    CGRect pickerFrame = self.amountViewContainer.frame;
    pickerFrame.origin.y = self.view.frame.size.height-self.amountViewContainer.frame.size.height;
    
    [UIView animateWithDuration:(animated?0.3f:0.0f) animations:^{
        self.amountViewContainer.frame = pickerFrame;
    } completion:^(BOOL finished) {
        if(completion) completion();
    }];
}

- (IBAction)done:(id)sender
{
    [self hideKeyboard];
    [self performBlock:^{
        [self hidePickerView:YES completion:^{
            [self dismissViewControllerAnimated:NO completion:^{
                
                
                NSNumber *amount = [NSNumber numberWithDouble:[[self.amountField.text stringByReplacingOccurrencesOfString:@"$ " withString:@""] doubleValue]];
                if(self.delegate) [self.delegate amountViewController:self amount:amount];
            }];
        }];
    } afterDelay:(self.isKeyboardShowing?0.20f:0.0f)];
}

- (IBAction)cancel:(id)sender
{
    [self hideKeyboard];
    [self performBlock:^{
        [self hidePickerView:YES completion:^{
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
    } afterDelay:(self.isKeyboardShowing?0.20f:0.0f)];
}

- (void)hideKeyboard
{
    [self.amountField resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardFrameForCurrentView = [self.amountViewContainer convertRect:keyboardFrame fromView:nil];
    
    CGRect payeeViewContainerFrame = self.amountViewContainer.frame;
    payeeViewContainerFrame.origin.y = self.view.bounds.size.height - (keyboardFrameForCurrentView.size.height + payeeViewContainerFrame.size.height);
    
    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState|(curve << 16) animations:^{
        self.amountViewContainer.frame = payeeViewContainerFrame;
    }completion:^(BOOL finished){
        self.isKeyboardShowing = YES;
    }];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    
    CGRect payeeViewContainerFrame = self.amountViewContainer.frame;
    payeeViewContainerFrame.origin.y = self.view.bounds.size.height - payeeViewContainerFrame.size.height;
    
    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState|curve animations:^{
        self.amountViewContainer.frame = payeeViewContainerFrame;
    }completion:^(BOOL finished){
        self.isKeyboardShowing = NO;
    }];
}

#pragma mark TextField delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (![newText hasPrefix:@"$ "])
    {
        return NO;
    }
    
    // Default:
    return YES;
}

@end
