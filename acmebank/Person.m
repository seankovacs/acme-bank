//
//  Person.m
//  acmebank
//
//  Created by Sean Kovacs on 4/12/14.
//  Copyright (c) 2014 SK DEV Solutions, LLC. All rights reserved.
//

#import "Person.h"

@implementation Person

- (NSString *)prettyName
{
    if(self.firstname.length > 0 && self.lastname.length > 0) {
        return [NSString stringWithFormat:@"%@ %@",[self.firstname capitalizedString],[self.lastname capitalizedString]];
    }
    
    return [NSString stringWithFormat:@"%@",[self.firstname capitalizedString]];
}

@end
